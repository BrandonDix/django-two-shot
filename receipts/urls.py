from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from receipts.views import (
    ReceiptsListView,
    ReceiptsCreateView,
    AccountsCreateView,
    CategoriesCreateView,
    ExpenseCategoryListView,
    AccountListView,
)

urlpatterns = [
    path("", ReceiptsListView.as_view(), name="home"),
    path("account/", AccountListView.as_view(), name="account"),
    path("categories/", ExpenseCategoryListView.as_view(), name="categories"),
    path("create/", ReceiptsCreateView.as_view(), name="receipts_create"),
    path(
        "accounts/create/",
        AccountsCreateView.as_view(),
        name="accounts_create",
    ),
    path(
        "categories/create/",
        CategoriesCreateView.as_view(),
        name="categories_create",
    ),
]
